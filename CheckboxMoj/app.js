class Question extends React.Component{
     state = {
        isConfirmed: false,
        isNotConfirmed: false,
        isFormSubmitted: false,
    }

    handleCheckboxConfirmed = () =>{
        this.setState({
            isConfirmed: !this.state.isConfirmed,
            isNotConfirmed: false,
            isFormSubmitted: false,
        })
    }
        
    handleCheckboxNotConfirmed = () =>{
        this.setState({
            isNotConfirmed: !this.state.isNotConfirmed,
            isConfirmed: false,
            isFormSubmitted: false,
        })
    }

    handleFormSubmit = (e) =>{
        e.preventDefault();
        if(!this.state.isFormSubmitted){
            this.setState({
                isFormSubmitted: !this.state.setState,
            })
        }
    }

    displayMessage = () => {    
        const {isFormSubmitted, isConfirmed, isNotConfirmed} = this.state
        if(isFormSubmitted){  
            if(isConfirmed){
                return <p>Dobre ziomencjusz</p>;
            }else if(isNotConfirmed){
                return <p>Ale dzban</p>
            }
        }else{
            return null;
        }
    }

    render(){
        const {isConfirmed, isNotConfirmed } = this.state
        return(

            <>
            <h1>Siemaneczko szybkie pytanie</h1>
            <OrderForm 
            changeConfirmed = {this.handleCheckboxConfirmed} 
            changeNotConfirmed = {this.handleCheckboxNotConfirmed}
            submit={this.handleFormSubmit} 
            checkedConfirm={ isConfirmed}
            checkedNotConfirm={isNotConfirmed}
            />
                
            {this.displayMessage()}
           
            </>
        )
    }
}


const OrderForm = (props) =>{
    const {submit, checkedConfirm, changeConfirmed, checkedNotConfirm,  changeNotConfirmed} = props
    
    return(
        <form onSubmit={submit}>
            <label htmlFor="ask">Czy lubisz placki?</label>
            <br/>
            <input type="radio" id="ask" checked={checkedConfirm} onChange={changeConfirmed}/>TAK
            <br/>
            <input type="radio" id="ask" checked={checkedNotConfirm} onChange={changeNotConfirmed} />NIE
            <br/>
            <button type="submit">Odpowiedź</button>            
        </form>
)}

ReactDOM.render(<Question />, document.getElementById('root'));